# rdesktop-fnmt

[rdesktop-fnmt][rdesktop-fnmt] es un contenedor basado en el
contenedor [rdesktop][linuxserver-rdesktop] de
[LinuxServer.io][linuxserver] que incluye los paquetes necesarios
proporcionados por la [FNMT][fnmt-configuracion] para la obtención y
uso del certificado digital para personas físicas.

[rdesktop-fnmt]: https://www.sede.fnmt.gob.es/certificados
[linuxserver-rdesktop]: https://github.com/linuxserver/docker-rdesktop
[linuxserver]: https://linuxserver.io
[fnmt-configuracion]: https://www.sede.fnmt.gob.es/certificados/persona-fisica/obtener-certificado-software/configuracion-previa

## Aviso Importante

Tenga en cuenta que rdesktop-fnmt se proporciona en las condiciones en
las que se encuentra, sin niguna garantía de no existencia de errores
o fallos.

Los autores no se hacen responsables de ningún daño o perjuicio
derivado del uso de rdesktop-fnmt o los contenedores generados por el
mismo. Utilice bajo su propio riesgo y responsabilidad.

## Por qué?

El uso del certificado digital de la FNMT en GNU/Linux siempre me ha
producido dolor de cabeza. Mi experiencia personal es que el soporte
siempre ha sido muy limitado y cada vez que quería utilizarlo para
hacer un trámite he necesitado hacer cambios en mi sistema. En el
"mejor" de los casos, simples cambios en la configuración de Firefox,
que es, prácticamente, el único navegador confiable en GNU/Linux para
su uso con el certificado. En el peor de los casos, instalación de
múltiples paquetes e, incluso, bajar versiones de alguno de ellos.

La irrupción de los contenedores ha posibilitado de manera trivial
usar un entorno congelado en una versión concreta para su ejecución en
un sistema anfitrión cambiante. De esta manera, podemos crear un
contenedor que incluya todo el software requerido por la FNMT con
versiones que sabemos que funcionan.

## Cómo?

Varios de los siguientes comandos utilizan [podman][podman] como motor
de contenedores pero son compatibles con [docker][docker]. Utiliza el
que más te guste.

Antes de usar el contenedor necesitamos crear un directorio para ser
utilizado como el directorio "home" que se utilizará dentro del
contenedor. Esto es importante porque queremos que los cambios locales
que ocurran usando el contenedor, especialmente en el perfil de
Firefox, no sean volátiles y podamos seguir usándolos en futuras
ejecuciones.

```
$ mkdir -p $HOME/rdesktop-fnmt-home
```

Para ejecutar el contenedor:

```
$ podman run -d \
  --name=rdesktop-fnmt \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/Madrid \
  -p 3389:3389 \
  -v $HOME/rdesktop-fnmt-home:/config \
  --shm-size="1gb" \
  --restart unless-stopped \
  tanty/rdesktop-fnmt
```

Como podemos ver, pasamos el directorio recientemente creado como
directorio `/config` dentro del contenedor. Para saber el significado
del resto de las opciones lee la [documentación][rdesktop-doc] del
contenedor rdesktop base.

Para conectarnos a la sesión de escritorio dentro del contenedor
necesitamos un cliente de [RDP][xrdp]. Por ejemplo:

```
$ rdesktop -g 90% localhost
```

En el momento de escribir esto, el usuario y la contraseña por defecto
son **abc/abc**. Recomiendo usar la sesión **Xorg**.

Cuando hayamos terminado de usar el contenedor podemos pararlo e,
incluso, eliminarlo.

```
$ podman stop rdesktop-fnmt
$ podman rm rdesktop-fnmt
```

[podman]: https://podman.io/
[docker]: https://www.docker.com/
[rdesktop-doc]: https://github.com/linuxserver/docker-rdesktop#parameters
[xrdp]: http://xrdp.org/

## DIY

Si quisiéramos construir nuestro propio contenedor a partir del
[Dockerfile][dockerfile] incluído en este repositorio, por ejemplo:

```
$ git clone https://gitlab.com/tanty/docker-rdesktop-fnmt.git
$ cd docker-rdesktop-fnmt
$ podman build \
  --no-cache \
  --pull \
  -t rdesktop-fnmt:latest \
  --build-arg BUILD_DATE=20211103 \
  --build-arg VERSION=0.1 .
```

Para usarlo, sólo tendríamos que reemplazar `tanty/rdesktop-fnmt` por
`rdesktop-fnmt:latest` en el comando para su ejecución.

[dockerfile]: ./Dockerfile
