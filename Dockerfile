FROM ghcr.io/linuxserver/rdesktop:ubuntu-xfce

# set version label
ARG BUILD_DATE
ARG VERSION
LABEL build_version="Tanty version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="tanty"

RUN \
 echo "**** install packages ****" && \
 apt-get update && \
 DEBIAN_FRONTEND=noninteractive \
 apt-get install --no-install-recommends -y \
	gconf2 \
	default-jre \
	libnss3-tools \
	unzip \
	libpcsclite1 \
	libcanberra-gtk-module \
  libcanberra-gtk3-module && \
 echo "**** install autofirma ****" && \
 curl -k -# -O -L https://estaticos.redsara.es/comunes/autofirma/1/7/1/AutoFirma_Linux.zip && \
 unzip AutoFirma_Linux.zip AutoFirma*.deb && \
 dpkg -i AutoFirma*.deb && \
 rm -f AutoFirma_Linux.zip AutoFirma*.deb && \
 echo "**** install configuradorfnmt ****" && \
 curl -k -# -O -L https://descargas.cert.fnmt.es/Linux/configuradorfnmt_1.0.1-0_amd64.deb && \
 dpkg -i configuradorfnmt_1.0.1-0_amd64.deb && \
 rm -f configuradorfnmt_1.0.1-0_amd64.deb && \
 echo "**** cleanup ****" && \
 apt-get purge -y unzip && \
 apt-get autoclean && \
 rm -rf \
        /var/lib/apt/lists/* \
        /var/tmp/* \
        /tmp/*
